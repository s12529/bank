
public class Account {
	
	private String number;
	private History history;
	private double amount;
		
	public double getAmount() {
		return this.amount;
	}
	
	public History getHistory() {
		return this.history;
	}
	
	public String getNumber() {
		return this.number;
	}

	public Account(String number) {
		this.number = number;
		this.history = new History();
		this.amount = 0;
	}
	
	public void add(double inAmount) {
		this.amount += inAmount;
		this.history.log(new HistoryLog("Kwota: " + inAmount, OperationType.INCOME));
	}
	
	public void subtract(double inAmount) {
		this.amount -= inAmount;
		this.history.log(new HistoryLog("Kwota: " + inAmount, OperationType.OUTCOME));
	}
	
	public void doOperation(Operation inOp) {
		inOp.execute(this);
	}
	
	
}
