
public class BankApp {

	public static void main(String[] args) {
		
		Bank bank = new Bank();
		
		Account acc1 = new Account("0000 1111 2222 3333 4444 5555");
		Account acc2 = new Account("1111 1111 2222 3333 4444 5555");
		Account acc3 = new Account("2222 1111 2222 3333 4444 5555");
		
		System.out.println("Witamy nowych klientow acc1, acc2, acc3.");		
		System.out.println("Wplacamy pieniazki.\n");
		
		bank.income(acc1, 300);
		bank.income(acc2, 200);
		bank.income(acc3, 250);
		
		System.out.println("acc1 ma " + acc1.getAmount() + " z�., nr konta: " + acc1.getNumber());
		System.out.println("acc2 ma " + acc2.getAmount() + " z�., nr konta: " + acc2.getNumber());
		System.out.println("acc3 ma " + acc3.getAmount() + " z�., nr konta: " + acc3.getNumber());
		
		System.out.println("\nacc1 przesy�a 100 z� do acc 2.\n");
		
		bank.transfer(acc1, acc2, 100);
		
		System.out.println("acc1 ma " + acc1.getAmount() + " z�.");
		System.out.println("acc2 ma " + acc2.getAmount() + " z�.");
		System.out.println("acc3 ma " + acc3.getAmount() + " z�.");
		
		System.out.println("\nHistoria operacji acc1\n");
		System.out.println(acc1.getHistory());
		System.out.println("\nHistoria operacji acc2\n");
		System.out.println(acc2.getHistory());
		System.out.println("\nHistoria operacji acc3\n");
		System.out.println(acc3.getHistory());
		
	}

}
