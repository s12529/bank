
public class Transfer extends Operation{
	
	private Account from;
	private Account to;
	private double amount;

	public Transfer(Account to, double amount) {
		this.setName("Transfer");
		this.to = to;
		this.amount = amount;
	}
	
	@Override
	public void execute(Account account) {
		this.from = account;
		this.from.subtract(amount);
		this.to.add(amount);	
	}	
	
}
