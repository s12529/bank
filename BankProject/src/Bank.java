
public class Bank {
	
	private Operation income;
	private Operation transfer;
		
	public void income(Account inAccount, double inAmount) {	
		income = new Income(inAmount);
		inAccount.doOperation(income);
	}
	
	public void transfer(Account inFrom, Account inTo, double inAmount) { 
		transfer = new Transfer(inTo, inAmount);
		inFrom.doOperation(transfer);	
	}
	
}
