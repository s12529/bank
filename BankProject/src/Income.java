
public class Income extends Operation {
	
	private Account account;
	private double amount;
	
	public Income(double amount) {
		this.setName("Wplata");
		this.amount = amount;
	}

	@Override
	public void execute(Account account) {
		this.account = account;
		this.account.add(amount);
	}

}
