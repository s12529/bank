import java.util.Date;

public class HistoryLog {

	private Date DateOfOperation;
	private String title;
	private OperationType operation;
	public static int listNumber;
		
	public HistoryLog(String title, OperationType operation) {
		DateOfOperation = new Date();
		this.title = title;
		this.operation = operation;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("\n" + listNumber + ". Data wykonania: " + DateOfOperation + ", "+ title + " z�, Operacja: " + operation.getOperationDesc());
		listNumber++;
		return builder.toString();
	}
	
	
	
	
	
}
